import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  starter: 0,
  status: null
}

const getters = {
  powerTwo: state => (state.starter * state.starter)
}

const mutations = {
  setValue (state, item) {
    state.starter += item
  },
  setText (state, item) {
    state.status = item
  }
}

const actions = {
  getValue ({ commit }, value) {
    let text = String()
    if (value === 1) {
      text = 'Increase Value'
      commit('setValue', value)
    } else if (value === -1) {
      text = 'Decrease Value'
      commit('setValue', value)
    }
    commit('setText', text)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
